import './App.css';
import { useSelector } from 'react-redux';
import { StyledEngineProvider } from '@mui/material';
// routing
import Routes from './routes';

function App() {

  //const customization = useSelector((state) => (state as any).customization);

  return (
    <StyledEngineProvider injectFirst>
      <Routes />
    </StyledEngineProvider>
  );
}

export default App;
