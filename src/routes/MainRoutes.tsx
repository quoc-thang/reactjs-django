
import { Route, Switch, useLocation } from "react-router-dom"
import AuthGuard from "../utils/route-guard/AuthGuard";
import Loadable from '../ui-component/Loadable';
import { lazy } from "react";
import MainContent from "../views/dashboard/default/MainContent";
import Dashboard1 from "../views/dashboard/default/component/Dashboard1";
const Dashboard = Loadable(lazy(() => import('../views/dashboard/default')));


const MainRoutes = () => {

    const location = useLocation();

    return (
        <Route path={'/dashboard'}>
            <Switch location={location} key={location.pathname}>
                <AuthGuard>
                    <Route path="/dashboard" component={Dashboard} />
                </AuthGuard>
            </Switch>
        </Route>

    )
}

export default MainRoutes;