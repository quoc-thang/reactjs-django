import React, { lazy } from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';

// project imports
import GuestGuard from './../utils/route-guard/GuestGuard';
//import MinimalLayout from './../layout/MinimalLayout';
//import NavMotion from './../layout/NavMotion';
import Loadable from '../ui-component/Loadable';

// login routing
//import AuthLogin from '../views/pages/authentication/login'
const AuthLogin = Loadable(lazy(() => import('../views/pages/authentication/login')));

//-----------------------|| AUTH ROUTING ||-----------------------//

const LoginRoutes = () => {
    const location = useLocation();

    return (
        <Route path={'/sign-in'}>
                <Switch location={location} key={location.pathname}>
                        <GuestGuard>
                            <Route path="/sign-in" component={AuthLogin} />
                        </GuestGuard>
                </Switch>
        </Route>
    );
};

export default LoginRoutes;
