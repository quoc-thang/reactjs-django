import BACKEND_SERVER from '../../config';

export const PostData = async (url: string, data: any) => {
    const response = await BACKEND_SERVER.post(url, data);
    if (response.status !== 200) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    const responseData = await response.data;
    return responseData;
}

export const GetData = async (url: string) => {
    const response = await BACKEND_SERVER.get(url);
    if (response.status !== 200) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    const responseData = await response.data;
    return responseData;
}
