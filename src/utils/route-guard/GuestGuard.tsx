import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

// project imports
import config from '../../config';
import Dashboard from '../../views/dashboard/default';

type Props = {
    children:any
}

//-----------------------|| GUEST GUARD ||-----------------------//

/**
 * Guest guard for routes having no auth required
 * @param {PropTypes.node} children children element/node
 */
const GuestGuard = (props: Props) => {
    const { children } = props;
    const account = useSelector((state) => (state as any).account);
    const { isLoggedIn } = account;

    if (isLoggedIn) {
        return (window.location.href = "/dashboard");
    }

    return children;
};

GuestGuard.propTypes = {
    children: PropTypes.node
};

export default GuestGuard;
