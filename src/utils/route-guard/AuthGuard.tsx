import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Login from '../../views/pages/authentication/login';

type Props = {
    children: any;
}

//-----------------------|| AUTH GUARD ||-----------------------//

/**
 * Authentication guard for routes
 * @param {PropTypes.node} children children element/node
 */
const AuthGuard = (props: Props) => {
    const { children } = props;
    const account = useSelector((state) => (state as any).account);
    const { isLoggedIn } = account;

    if (!isLoggedIn) {
        return window.location.href = '/sign-in';
    }

    return children;
};

export default AuthGuard;
