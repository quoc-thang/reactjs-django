import React from 'react'

type Props = {
    subMenuItems: ItemData[];
}

type ItemData = {
    name: string,
    path: string,
}

function SubMenu(props: Props) {
    const { subMenuItems } = props;

    if (!subMenuItems.length) {
        return null;
    }
    return (
        <ul className="nav nav-treeview">
            {subMenuItems.map((item) => (
                <li key={item.name} className="nav-item">
                    <a href={item.path} className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>{item.name}</p>
                    </a>
                </li>
            ))}
        </ul>
    )
}

export default SubMenu