import { useMemo, useState } from "react";
import SubMenu from "./SubMenu";

function Menu() {

    const [menuItem, setMenuItem] = useState([{
        name: '',
        path: '',
        icon: '',
        subMenuItems: [
            {
                name: '',
                path: ''
            }
        ]
    }]);

    useMemo(() => {
        // TODO: Get data from DB
        setMenuItem([
            {
                name: 'Dashboard',
                path: '/dashboard',
                icon: 'fa-tachometer-alt',
                subMenuItems: [
                    {
                        name: 'Dashboard v1',
                        path: '/dashboard/v1'
                    },
                    {
                        name: 'Dashboard v2',
                        path: '/dashboard/v2'
                    }
                ]
            },
            {
                name: 'Widgets',
                path: '/dashboard/placeholder',
                icon: 'fa-th',
                subMenuItems: []
            },
            {
                name: 'Layout Options',
                path: '/dashboard/placeholder',
                icon: 'fa-copy',
                subMenuItems: []
            },
            {
                name: 'Charts',
                path: '/dashboard/placeholder',
                icon: 'fa-chart-pie',
                subMenuItems: []
            },
            {
                name: 'UI Elements',
                path: '/dashboard/placeholder',
                icon: 'fa-tree',
                subMenuItems: []
            },
            {
                name: 'Forms',
                path: '/dashboard/placeholder',
                icon: 'fa-edit',
                subMenuItems: []
            },
            {
                name: 'Tables',
                path: '/dashboard/placeholder',
                icon: 'fa-table',
                subMenuItems: []
            }
        ])
    },[])

    return (
        <div>
            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                {/* Brand Logo */}
                <a href="/dashboard" className="brand-link">
                    <img src="/dist/img/systemexe-logo.png" alt="AdminLTE Logo" className="brand-image elevation-3" style={{ opacity: '.8' }} />
                    <span className="brand-text font-weight-light">SystemEXE</span>
                </a>
                {/* Sidebar */}
                <div className="sidebar">
                    {/* Sidebar user panel (optional) */}
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div className="image">
                            <img src="/dist/img/avatar.jpg" className="img-circle elevation-2" alt="User Image" />
                        </div>
                        <div className="info">
                            <a href="#" className="d-block">Nguyễn Phụng Tiên</a>
                        </div>
                    </div>
                    {/* SidebarSearch Form */}
                    <div className="form-inline">
                        <div className="input-group" data-widget="sidebar-search">
                            <input className="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" />
                            <div className="input-group-append">
                                <button className="btn btn-sidebar">
                                    <i className="fas fa-search fa-fw" />
                                </button>
                            </div>
                        </div>
                    </div>
                    {/* Sidebar Menu */}
                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">                    
                            {
                               menuItem.map((item) => (
                                <li key={item.name} className="nav-item menu-open">
                                    <a href={item.path} className="nav-link">
                                    <i className={`nav-icon fas  ${item.icon}`}  />
                                    <p>
                                        {item.name}
                                        <i className="right fas fa-angle-left" />
                                    </p>
                                </a>
                                <SubMenu subMenuItems={item.subMenuItems} />
                                </li>    
                               )) 
                            }    
                        </ul>
                    </nav>
                    {/* /.sidebar-menu */}
                </div>
                {/* /.sidebar */}
            </aside>

        </div>
    )
}

export default Menu;