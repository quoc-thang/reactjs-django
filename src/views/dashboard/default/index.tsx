//-----------------------|| DEFAULT DASHBOARD ||-----------------------//

import Menu from "./Menu";
import Header from "./Header";
import MainContent from "./MainContent";
import Footer from "./Footer";

type Props = {
    location: {pathname: string};
}

const Dashboard = (props: Props) => {

    return (
        <div className="wrapper">
            <Header />
            <Menu />
            <MainContent action={props.location.pathname} />
            <Footer />
        </div>
    );
};

export default Dashboard;
