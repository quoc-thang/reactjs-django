import React, { useEffect, useState } from "react";
import BACKEND_SERVER from '../../../../config';
import configData from '../../../../config';
import styles from './login.module.css';
import { notify_fail, notify_success } from "../../../../client/common/Notification";
import { useDispatch } from 'react-redux';
import { GetData, PostData } from "../../../../utils/httpClient/HttpClient";

const Login = () => {
    // login
    const [nameLogin, setNameLogin] = useState('');
    const [passwordLogin, setPasswordLogin] = useState('');
    const [rememberLogin, setRememberLogin] = useState(true);

    // sign_up
    const [nameSignUp, setNameSignUp] = useState('');
    const [emailSignUp, setEmailSignUp] = useState('');
    const [passwordSignUp, setPasswordSignUp] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');

    //valid
    const [valid, setValid] = useState({ valNameSignUp: 'none', valVerifyPassword: 'none' });

    // sign_up or login
    const [slideUp, setSlideUp] = useState({ signup: styles.slide_up, login: '' })

    const dispatcher = useDispatch();

    // event sign_up
    const handleSignUp = (event: any) => {
        event.preventDefault();
        if (passwordSignUp === verifyPassword) {
            PostData(`/postUser`, {
                user_name: nameSignUp,
                email: emailSignUp,
                password: passwordSignUp,
            }).then((responseData) => {
                notify_success("sign-up");
            }).catch((error) => notify_fail("sign-up"));
        }
        else {
            alert('Verify password does not match!');
        }
    }

    // event login
    const handleLogin = (event: any) => {
        event.preventDefault();
        PostData(`/getToken`, {
            user_name: nameLogin,
            password: passwordLogin
        })
            .then((responseData) => {
                notify_success("Login");
                dispatcher({
                    type: 'ACCOUNT_INITIALIZE',
                    payload: { isLoggedIn: true, user: responseData.user, token: responseData.token }
                });
            })
            .catch((error) => notify_fail("Login"));
    }

    const handleCheckUser = (value: any) => {
        setNameSignUp(value);
        GetData(`/checkUserName?user_name=${value}`)
        .then((responseData) => {
            setValid({ ...valid, valNameSignUp: 'none' });
        }).catch((error) => {
            setValid({ ...valid, valNameSignUp: 'block' });
        });
    }

    const checkVerifyPassword = (verify: any, value: any) => {
        if (verify) {
            setVerifyPassword(value);
            if (value === passwordSignUp) {
                setValid({ ...valid, valVerifyPassword: 'none' });
            } else {
                setValid({ ...valid, valVerifyPassword: 'block' });
            }
        } else {
            setPasswordSignUp(value);
            if (value === verifyPassword) {
                setValid({ ...valid, valVerifyPassword: 'none' });
            } else {
                setValid({ ...valid, valVerifyPassword: 'block' });
            }
        }
    }

    return (
        <div className="container">
            <div className={styles.form_structor}>
                {/* sign_up */}
                <form onSubmit={handleSignUp}>
                    <div className={styles.signup + " " + slideUp.signup}>
                        <h2 onClick={() => setSlideUp({ ...slideUp, signup: '', login: styles.slide_up })} className={styles.form_title}><span>or</span>Sign up</h2>
                        <div className={styles.form_holder}>
                            {/* <input required type="text" className={styles.input} placeholder="Name" onChange={e => { setNameSignUp(e.target.value) }} /> */}
                            <input
                                id="nameSignUp"
                                required
                                type="text"
                                className={styles.input}
                                placeholder="Name"
                                onChange={e => handleCheckUser(e.target.value)} />
                            <label
                                className={styles.checkValid}
                                htmlFor="nameSignUp"
                                style={{ display: valid.valNameSignUp }}>User Name Exist!</label>
                            <input
                                required
                                type="email"
                                className={styles.input}
                                placeholder="Email"
                                onChange={e => { setEmailSignUp(e.target.value) }} />
                            <input
                                required
                                type="password"
                                className={styles.input}
                                placeholder="Password"
                                onChange={e => { checkVerifyPassword(false, e.target.value) }} />
                            <input
                                id="verifyPassword"
                                required
                                type="password"
                                className={styles.input}
                                placeholder="Verify password"
                                onChange={e => { checkVerifyPassword(true, e.target.value) }} />
                            <label
                                className={styles.checkValid}
                                htmlFor="verifyPassword"
                                style={{ display: valid.valVerifyPassword }}>
                                Verify password does not match
                            </label>
                        </div>
                        <button type="submit" className={styles.submit_btn}>Sign up</button>
                    </div>
                </form>
                {/* login */}
                <form onSubmit={handleLogin}>
                    <div className={styles.login + " " + slideUp.login}>
                        <div className={styles.center}>
                            <h2 onClick={() => setSlideUp({ ...slideUp, signup: styles.slide_up, login: '' })} className={styles.form_title}><span>or</span>Log in</h2>
                            <div className={styles.form_holder}>
                                {/* user name */}
                                <input
                                    required
                                    type="text"
                                    className={styles.input}
                                    placeholder="Name"
                                    value={nameLogin}
                                    onChange={e => { setNameLogin(e.target.value) }} />
                                {/* password */}
                                <input
                                    required
                                    minLength={6}
                                    type="password"
                                    className={styles.input}
                                    placeholder="Password to 6 characters or more"
                                    value={passwordLogin}
                                    onChange={e => { setPasswordLogin(e.target.value) }} />
                            </div>
                            <div>
                                {/* checkbox remember */}
                                <label className={styles.remember_checkbox}>
                                    <input type="checkbox" defaultChecked={rememberLogin} onChange={e => { setRememberLogin(e.target.checked) }} />
                                    <span className={styles.remember_checkmark}>Remember</span>
                                </label>
                            </div>
                            {/* button log_in */}
                            <button type="submit" className={styles.submit_btn} >Log in</button>
                        </div>
                    </div>
                </form>
            </div >
        </div>
    )
}

export default Login;