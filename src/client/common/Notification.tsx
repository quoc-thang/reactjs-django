import { toast } from "react-toastify";

export const notify_success = (message: string) => {
    toast.success(message + " success!", {
        position: toast.POSITION.TOP_RIGHT
    });
}

export const notify_fail = (message: string) => {
    toast.error(message + " fail!", {
        position: toast.POSITION.TOP_RIGHT
    });
}

export const notify_warn = (message: string) => {
    toast.warn(message + " warn!", {
        position: toast.POSITION.TOP_RIGHT
    });
}

export const notify_info = (message: string) => {
    toast.info(message, {
        position: toast.POSITION.TOP_RIGHT
    });
}
