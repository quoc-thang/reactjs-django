// material-ui
import LinearProgress from '@mui/material/LinearProgress';

//-----------------------|| Loader ||-----------------------//

const Loader = () => {
    return (
        <div className="loader">
            <LinearProgress color="primary" />
        </div>
    );
};

export default Loader;
